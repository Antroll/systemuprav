# site
Десктопная версия сайта **[site.com](#)**

#### Установка
Необходимо глобально установить Gulp и Bower:

```sh
$ npm install -g gulp bower
```

```sh
$ git clone git@gitlab.com:Antroll/new-project.git
$ cd new-project
$ npm install && bower install
```

Для просмотра проекта:

```sh
$ gulp serve
```

Исходники проекта содержаться в папке **project/dist**
