$(document).ready(function () {
	/*slick init*/
	$('.slider__slick').slick({
		arrows: true,
		infinite: true,
		speed: 300,
		fade: true,
		cssEase: 'linear',
		appendArrows: $(".slider__buttons") 
	});
});